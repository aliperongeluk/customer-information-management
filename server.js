const swaggerUi = require('swagger-ui-express');
const swaggerEnv = require('./src/environment/config/swagger');
const mongo = require('./src/connections/mongodb.connection');
const actuator = require('./src/actuator/actuatorMiddleware');

const express = require('express');
const app = express();
const port = process.env.PORT || 7003;

const customerRoutes = require('./src/routes/customer.routes');
const messageSender = require('./src/eventhandlers/message.sender');

const bodyParser = require('body-parser');

const rabbitmq = require('./src/connections/rabbitmq.connection');
const { eurekaClient } = require('./src/environment/config/eureka.config');

app.use(bodyParser.json());

app.use(actuator('/actuator'));

// routes:
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerEnv));
app.use('/customers', customerRoutes);

// default:
app.use('*', (req, res) => {
  res.status(404).send({
    error: 'not available',
  });
});

rabbitmq
  .connect()
  .then(channels => {
    messageSender.setMessageChannel(channels.sendChannel);
  })
  .catch(err => {
    // eslint-disable-next-line no-console
    console.error(err);
  });

if (process.env.NODE_ENV === 'production') {
  // eslint-disable-next-line no-console
  console.log('Starting eureka connection...');
  eurekaClient.logger.level('debug');
  eurekaClient.start();
}

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server running on port [${port}]`);
});
