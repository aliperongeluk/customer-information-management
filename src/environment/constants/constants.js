const constants = {
  EXCHANGE_CUSTOMER_INFORMATION: 'customer-information',
  SERVICE_NAME: 'customer-information-management',
};

module.exports = constants;
