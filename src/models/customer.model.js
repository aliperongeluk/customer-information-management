const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CustomerSchema = new Schema(
  {
    authId: {
      type: Schema.Types.ObjectId,
      required: [
        true,
        'An id from the authentication microservice is required',
      ],
    },
    email: {
      type: String,
      required: [true, 'Email is required'],
    },
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    phoneNumber: {
      type: String,
    },
    street: {
      type: String,
    },
    postalCode: {
      type: String,
    },
    city: {
      type: String,
    },
    country: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

const Customer = mongoose.model('customer', CustomerSchema);

module.exports = Customer;

/**
 * @swagger
 * definitions:
 *  Customer:
 *    type: object
 *    properties:
 *      authId:
 *        type: string
 *      email:
 *        type: string
 *      firstName:
 *        type: string
 *      lastName:
 *        type: string
 *      phoneNumber:
 *        type: string
 *      street:
 *        type: string
 *      postalCode:
 *        type: string
 *      city:
 *        type: string
 *      country:
 *        type: string
 */
