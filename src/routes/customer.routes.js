const express = require('express');
const routes = express.Router();

const Customer = require('../models/customer.model');
const messageSender = require('../eventhandlers/message.sender');
const events = require('../environment/constants/events');

/**
 * @swagger
 * /api/customer-information-management/customers/{id}:
 *    get:
 *     tags:
 *       - customers (microservice mongo)
 *     description: Returns all customers
 *     produces:
 *       - application/json
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: String Auth ID of the customer.
 *     responses:
 *       200:
 *          description: Successfully loaded customers.
 *          schema:
 *            $ref: '#/definitions/Customer'
 *       400:
 *          description: Failed to load customers.
 */
routes.get('/:id', (req, res) => {
  const id = req.params.id;

  Customer.findOne({ authId: id })
    .then(customer => {
      if (customer != null) {
        res.status(200).send(customer);
      } else {
        res.status(404).send({
          message: 'Customer not found.',
        });
      }
    })
    .catch(error => {
      res.status(400).send(error);
    });
});

/**
 * @swagger
 * /api/customer-information-management/customers:
 *    get:
 *     security:
 *       - bearerAuth: []
 *     tags:
 *       - customers (microservice mongo)
 *     description: Returns all customers
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Successfully loaded customers.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Customer'
 *       400:
 *          description: Failed to load customers.
 */
routes.get('/', (req, res) => {
  Customer.find({})
    .then(customers => {
      res.status(200).send(customers);
    })
    .catch(error => {
      res.status(400).send(error);
    });
});

/**
 * @swagger
 * /api/customer-information-management/customers:
 *    post:
 *      tags:
 *        - customers (microservice mongo)
 *      description: Creates a customer (soon only possible through authentication microservice)
 *      produces:
 *        - application/json
 *      parameters:
 *        - in: body
 *          name: body
 *          description: Set initial customer attributes
 *          schema:
 *            $ref: '#/definitions/Customer'
 *      responses:
 *        200:
 *          description: Customer added successfully.
 *          schema:
 *            $ref: '#/definitions/Customer'
 *        400:
 *          description: Failed to create customer.
 */
routes.post('/', (req, res) => {
  Customer.findOne({ authId: req.body.authId })
    .then(customer => {
      if (customer == null) {
        const newCustomer = new Customer(req.body);

        Customer.findOne({ email: req.body.email })
          .then(customer => {
            if (customer == null) {
              newCustomer
                .save()
                .then(createdCustomer => {
                  messageSender.publish(
                    events.USER_REGISTERED,
                    createdCustomer
                  );
                  res.status(200).send(createdCustomer);
                })
                .catch(error => {
                  res.status(500).send(error);
                });
            } else {
              res.status(400).send({
                message:
                  'Something went wrong. Possibly, a user with the given email already exists.',
              });
            }
          })
          .catch(error => {
            res.status(500).send(error);
          });
      } else {
        res.status(400).send({
          message:
            'Something went wrong. Possibly, a user with the given authId already exists.',
        });
      }
    })
    .catch(error => {
      res.status(500).send(error);
    });
});

/**
 * @swagger
 * /api/customer-information-management/customers/{id}:
 *    put:
 *      tags:
 *        - customers (microservice mongo)
 *      description: Creates a customer (soon only possible through authentication microservice)
 *      produces:
 *        - application/json
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: String ID of the customer.
 *        - in: body
 *          name: body
 *          description: Update customer attributes
 *          schema:
 *              $ref: '#/definitions/Customer'
 *      responses:
 *        200:
 *          description: Changes were successfully made.
 *          schema:
 *            $ref: '#/definitions/Customer'
 *        400:
 *          description: Failed to make changes to customer.
 */
routes.put('/:id', (req, res) => {
  const id = req.params.id;
  delete req.body.authId;

  Customer.findById(id)
    .then(customer => {
      customer
        .updateOne(req.body)
        .then(() => {
          Customer.findById(id)
            .then(customer => {
              messageSender.publish(events.USER_UPDATED, customer);
              res.status(200).send(customer);
            })
            .catch(error => {
              // eslint-disable-next-line no-console
              console.log(error);
              res.status(400).send(error);
            });
        })
        .catch(error => {
          res.status(400).send(error);
        });
    })
    .catch(error => {
      res.status(400).send(error);
    });
});

module.exports = routes;
