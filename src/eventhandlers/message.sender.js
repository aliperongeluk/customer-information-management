let messageChannel;
const constants = require('../environment/constants/constants');

const publish = (topic, payload) => {
  messageChannel.publish(
    constants.EXCHANGE_CUSTOMER_INFORMATION,
    topic,
    Buffer.from(JSON.stringify(payload)),
    {
      persistent: true,
    }
  );
};

const setMessageChannel = channel => {
  messageChannel = channel;
};

module.exports = { setMessageChannel, publish };
